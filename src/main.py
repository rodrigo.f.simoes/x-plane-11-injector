import struct
import time

from src.socket import UDPSocket, socket_send

X_PLANE_IP = "192.168.80.2"  # Prod: 192.168.80.2 Test: 192.168.1.81
X_PLANE_PORT = 49000

UPDATE_RATE = 10  # Minutes


def build_message_dataref(dataref: str, value: float) -> bytes:
    return struct.pack('<4sxf500s', b'DREF', value, dataref.encode('utf-8'))


def send_to_xplane(socket, message: bytes) -> None:
    socket_send(socket=socket, ip=X_PLANE_IP, port=X_PLANE_PORT, message=message)


def main():
    with UDPSocket(name="Inputs Listener") as socket:
        tank0_message = build_message_dataref(dataref="sim/flightmodel/weight/m_fuel1", value=50.0)
        tank1_message = build_message_dataref(dataref="sim/flightmodel/weight/m_fuel2", value=50.0)

        while True:
            send_to_xplane(socket, tank0_message)
            send_to_xplane(socket, tank1_message)
            time.sleep(UPDATE_RATE * 60)

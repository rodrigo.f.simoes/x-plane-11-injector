from socket import socket, AF_INET, SOCK_DGRAM, error

from src.logging import get_logger

logger = get_logger(__name__)


class UDPSocket:
    def __init__(self, name: str = "GiveItAName!"):
        """name: str is for debugging -> know who is calling"""
        self.name = name
        try:
            self.socket = socket(AF_INET, SOCK_DGRAM)
            logger.info(f"[{self.name}] UDP Socket open")
        except error as err:
            logger.error(msg=f"[{self.name}] UDP Socket creation failed", exc_info=err)

    def __enter__(self):
        return self.socket

    def __exit__(self, type, value, traceback):
        self.socket.close()
        logger.info(f"[{self.name}] UDP Socket closed")


def socket_send(socket: socket, ip: str, port: int, message: bytes) -> int:
    """Send data to the socket. Returns the number of bytes sent"""
    logger.debug(f"Worker just sent (preview) {message[:40]} to {ip}:{port}")
    return socket.sendto(message, (ip, port))
